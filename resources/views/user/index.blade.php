@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de usuarios</h1>
      <div class="alert">
        <a href="/users/create" class="btn btn-primary">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Role</th>
            <th>Operaciones</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>

              <form method="post" action="/users/{{ $user->id }}">
                <a class="btn btn-primary"  role="button"
                href="/users/{{ $user->id }}/edit">
                Editar
              </a>
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="borrar" class="btn btn-primary">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>

    {{ $users->render() }}
  </div>
</div>
</div>
@endsection
