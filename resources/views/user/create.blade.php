@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')



<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Alta de usuarios</h1>
      @if ($errors->any())
      <div class="alert alert-danger">
        <strong>
          Se han producido errores de validación
        </strong>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif



      <form class="form-horizontal" role="form" method="post" action="/users">

        {{ csrf_field() }}

        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="col-md-4  col-form-label text-md-right">
            Nombre
          </label>

          <div class="col-md-8">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{ old('name') }}" {{--required--}} autofocus>

            @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
          </div>
        </div>



        <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="col-md-4  col-form-label text-md-right">
            Email
          </label>

          <div class="col-md-8">
            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email" value="{{ old('email') }}" {{--required--}} autofocus>

            @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
        </div>




        <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
          <label for="email" class="col-md-4  col-form-label text-md-right">
            Color (prueba select)
          </label>

          <div class="col-md-8">
            <?php
            $example = ['rojo', 'azul', 'verde'];
            $html = '<hr>';
            ?>
            <select id="color" class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}"  name="color" value="{{ old('color') }}" autofocus>

              @foreach ($example as $item)
              <option value="{{ $item }}"
              {{ old('color') == $item ?
              'selected="selected"' : '' }}>
              {{ $item }}
            </option>
            @endforeach
          </select>
          @if ($errors->has('color'))
          <span class="help-block">
            <strong>{{ $errors->first('color') }}</strong>
          </span>
          @endif
        </div>
      </div>



      <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4  col-form-label text-md-right">
          Contraseña
        </label>

        <div class="col-md-8">
          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  name="password" value="{{ old('password') }}" {{--required--}} autofocus>

          @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        </div>
      </div>


      <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
          <button type="submit" class="btn btn-primary">
            {{-- __('New')--}}
            @lang('New')
          </button>
        </div>
      </div>

    </form>
  </div>
</div>
</div>
@endsection
